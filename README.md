# Login

//

Create Flask application, which will show login form and allow user to "log in", checking the username and password against saved one. Username and password can be saved in a text file, placed in the same folder as your script.

V1: File could contain two lines - username and password


Please, create a git repo for the project, upload it to the gitlab, and don't forget to do commits during your work!
V2: 
* File should contain JSON array of usernames, passwords and full names
* After successful login, the user should see a greeting - like "Hello, Serhii!" 
V3
* Login page and greeting page and all other pages should be done using templates - see documentation link below
* After successful login, user should come to another form with value input field, where she can calculate factorials for different numbers (not only once, but many times)

//

