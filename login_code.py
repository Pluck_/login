from flask import Flask, request, render_template
import json

f = open('User.json')
data = json.load(f)


app = Flask(__name__)

def check_login(a, b)->bool:
    return (a is data['username']) and (b is data['password'])\

def fac(n):
    if n == 1:
        return 1
    return n * fac(n-1)


@app.route("/")
def main():
    return render_template("main.html")

@app.route("/login")
def test_action1():
    val1 = request.args.get('value1')
    val2 = request.args.get('value2')
    if check_login(str(val1), str(val2)):
        return render_template("main.html", value1 = val1)

@app.route("/")
def main():
    return render_template("fac.html")

@app.route("/fac")
def test_action2():
    val = request.args.get('value3')
    res = fac(int(val))
    return render_template("fac.html", value3 = res)





